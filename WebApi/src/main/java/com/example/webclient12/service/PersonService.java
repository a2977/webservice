package com.example.webclient12.service;

import com.example.webclient12.controller.PersonRepository;
import com.example.webclient12.domain.PersonEntity;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class PersonService {
    @Autowired
    PersonRepository personRepository;

    @SneakyThrows
    public PersonEntity getById(String id){
        return personRepository.findById(id).orElseThrow(() -> new Exception("person id: " + id +" not found!  "  ));
    }

    public Stream<PersonEntity> getAllPersons(){
        return personRepository.findAll().stream();
    }
}

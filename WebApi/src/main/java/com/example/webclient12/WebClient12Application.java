package com.example.webclient12;

import com.example.webclient12.controller.PersonRepository;
import com.example.webclient12.domain.PersonEntity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WebClient12Application {


    public static void main(String[] args) {
        SpringApplication.run(WebClient12Application.class, args);
    }


    @Bean
    public CommandLineRunner dummyData(PersonRepository repository){
        return args -> {
            repository.save(new PersonEntity("caroline", "a", null));
            repository.save(new PersonEntity("zakhida", "a", null));
            repository.save(new PersonEntity("jakob", "a", null));
            repository.save(new PersonEntity("teo", "a", null));
            repository.save(new PersonEntity("linnea", "a", null));
            repository.save(new PersonEntity("niclas","a", null));
        };
    }
}

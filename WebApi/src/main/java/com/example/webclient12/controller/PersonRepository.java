package com.example.webclient12.controller;


import com.example.webclient12.domain.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, String> {
    List<PersonEntity> findByFirstName(String firstName);
}

package com.example.webclient12.controller;

import com.example.webclient12.domain.PersonEntity;
import com.example.webclient12.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {
    PersonService service;

    @PostMapping("students")
    public Stream<PersonEntity> GetAllStudent(){
        return service.getAllPersons();
    }
}

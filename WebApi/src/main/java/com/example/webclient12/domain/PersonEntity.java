package com.example.webclient12.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity(name="person")
public class PersonEntity {
    @Id
    String id ;
    String firstName;
    String lastName;

    @ElementCollection
    protected List<PersonEntity> Groups;

    public PersonEntity(String firstName, String lastName, List<PersonEntity> groups) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        Groups = groups;
    }
}

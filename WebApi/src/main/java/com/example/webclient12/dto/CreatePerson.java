package com.example.webclient12.dto;

import com.example.webclient12.domain.PersonEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;



import java.util.List;
import java.util.UUID;

@Value
public class CreatePerson   {
    String id;
    String firstName;
    String  lastName;
    List<PersonEntity> Groups;


    @JsonCreator
    public CreatePerson(
            @JsonProperty String firstName,
            @JsonProperty String lastName,
            @JsonProperty List<PersonEntity> Groups
    ){
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.Groups = Groups;

    }


}
